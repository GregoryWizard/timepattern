package com.gregory.timepatterns;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by root on 1/17/16.
 */
public class PrintActivity extends AppCompatActivity {
    public static final String DEFAULT = "NONE";

    private ListView mainListView;
    private ArrayAdapter<String> listAdapter;

    public  String[] p_names;
    public  int[] p_reps;
    public  String[] p_times;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.print_layout);

        mainListView = (ListView) findViewById( R.id.print );
        ArrayList<String> planetList = new ArrayList<String>();

        // Create ArrayAdapter
        listAdapter = new ArrayAdapter<String>(this, R.layout.textviewlist, planetList);

        getNamesAndTime();

    }

    public void getNamesAndTime() {
        SharedPreferences shared = getSharedPreferences("myData", Context.MODE_PRIVATE);
        String n  = shared.getString("names",      DEFAULT);
        String r  = shared.getString("reps",       DEFAULT);
        String st = shared.getString("stack_time", DEFAULT);
        String d  = shared.getString("day",        DEFAULT);

        if(st.equals(DEFAULT)) {
            listAdapter.add("Nothing to see.");
        } else {
            p_names = getArrayFromString(n);
            p_reps = getIntArrayFromString(r);
            p_times = getArrayFromString(st);

            for(int i=0;i<p_times.length;i++) {
                Log.d("P_TIMES",p_times[i]);
            }

            for(int i=0;i<p_times.length;i++) {
                listAdapter.add(p_times[i]);
            }
        }


        // Set the ArrayAdapter as the ListView's adapter.
        mainListView.setAdapter(listAdapter);

    }

    private String[] getArrayFromString(String _s) {
        int count=0;
        char[] text = _s.toCharArray();

        for(int i=0;i<text.length;i++) {
            if( text[i] == (',') ) {
                count++;
            } else if((i+1) == text.length ){
                count++;
            }
        }
        String[] ARR = new String[count];

        count=0;
        String buf=""; String result="";
        for(int i=0;i<text.length;i++) {
            if( text[i] == (',') ) {
                result += buf;
                ARR[count] = result;

                count++;
                buf = result = "";
            } else if((i+1) == text.length ){
                buf += text[i];
                result += buf;

                ARR[count] = result;
                buf = result = "";
            } else {
                buf += text[i];
            }
        }

        return ARR;
    }

    private void updateData(String key,String value) {
        SharedPreferences shared = getSharedPreferences("myData", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = shared.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public int[] getIntArrayFromString(String _s) {
        int count=0;
        char[] text = _s.toCharArray();

        for(int i=0;i<text.length;i++) {
            if( text[i] == (',') ) {
                count++;
            } else if((i+1) == text.length ){
                count++;
            }
        }
        int[] ARR = new int[count];

        count=0;
        String buf=""; String result="";
        for(int i=0;i<text.length;i++) {
            if( text[i] == (',') ) {
                int number=0;
                result += buf;
                try {
                    number = Integer.parseInt(result);
                } catch (Exception e) {

                }
                ARR[count] = number;

                count++;
                buf = result = "";
            } else if((i+1) == text.length ){
                int number=0;
                buf += text[i];
                result += buf;
                try {
                    number = Integer.parseInt(result);
                } catch (Exception e) {

                }
                ARR[count] = number;
                buf = result = "";
            } else {
                buf += text[i];
            }
        }

        return ARR;
    }


    @Override
    public void onBackPressed()
    {
        Intent intent = new Intent(getBaseContext(),MainActivity.class);
        startActivity(intent);
    }
}
