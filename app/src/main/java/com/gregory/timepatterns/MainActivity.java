package com.gregory.timepatterns;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by root on 1/17/16.
 */
public class MainActivity extends AppCompatActivity {
    // --------------------------------------------------------------------------------
    public static final String DEFAULT = "NONE";
    public static final int    ID_ROW  = 100;
    public static final int   ID_CELL  = 400;
    public int countRows=0;
    public int countCells=0;
    // --------------------------------------------------------------------------------
    public TableRow ROWS;
    public EditText[] CELLS;
    public EditText currentEditor;
    // --------------------------------------------------------------------------------
    TableLayout table;
    Button start, stop, print, about;
    volatile Thread thread;
    // --------------------------------------------------------------------------------
    boolean runningService = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_layout);

        initializeViews();

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
    }

    private void initializeViews() {
        // initialize
        table = (TableLayout) findViewById(R.id.tableLayout1);
        start = (Button) findViewById(R.id.start);
        stop  = (Button) findViewById(R.id.stop);
        print = (Button) findViewById(R.id.print);
        about = (Button) findViewById(R.id.about);

        // listeners
        start.setOnClickListener(new StartListener());
        stop.setOnClickListener(new StopListener());
        print.setOnClickListener(new PrintListener());
        about.setOnClickListener(new AboutListener());

        // data to editTexts
        addDataToViews();
    }

    private void addDataToViews() {
        SharedPreferences shared = getSharedPreferences("myData", Context.MODE_PRIVATE);
        String str_first   = shared.getString("first", DEFAULT);
        String str_names   = shared.getString("names",DEFAULT);
        String str_times   = shared.getString("times",DEFAULT);
        String str_beeps   = shared.getString("beeps",DEFAULT);
        String str_reps    = shared.getString("reps",DEFAULT);

        if(str_first.equals(DEFAULT))
        {
            Log.d("DEFAULT", "FALSE");
            String s_names = getResources().getString(R.string.names);
            String s_times = getResources().getString(R.string.times);
            String s_beeps = getResources().getString(R.string.beeps);
            String s_reps  = getResources().getString(R.string.reps);

            String[] only_names = getArrayFromString(s_names);
            String[] only_times = getArrayFromString(s_times);
            String[] only_beeps = getArrayFromString(s_beeps);
            String[] only_reps  = getArrayFromString(s_reps);

            updateData("names",s_names);
            updateData("times",s_times);
            updateData("beeps",s_beeps);
            updateData("reps", s_reps );

            for(int i=0;i<only_names.length;i++) {
                Log.d("ARR[" + Integer.toString(i) + "]: ", only_names[i]);
            }

            int COUNT = only_names.length;
            CELLS = new EditText[4];

            for(int i=0;i<COUNT;i++)
            {
                ROWS = new TableRow(this);
                ROWS.setId(ID_ROW + countRows);
                int padding = (int) getResources().getDimension(R.dimen.padding);
                ROWS.setPadding(padding, 0, padding, 0);
                ROWS.setGravity(Gravity.CENTER_HORIZONTAL);

                for(int j=0;j<4;j++) {
                    String TEXT="";
                    switch (j) {
                        case 0:
                            TEXT = only_names[i];
                            break;
                        case 1:
                            TEXT = only_times[i];
                            break;
                        case 2:
                            TEXT = only_beeps[i];
                            break;
                        case 3:
                            TEXT = only_reps[i];
                            break;
                    }
                    CELLS[j] = new EditText(this);
                    CELLS[j].setId(ID_CELL + countCells);
                    CELLS[j].setText(TEXT);

                    CELLS[j].setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT, 1f));
                    CELLS[j].setOnTouchListener(new EditTextListener());
                    CELLS[j].addTextChangedListener(new EditTextChangeListener());

                    ROWS.addView(CELLS[j]);
                    countCells++;
                }

                table.addView(ROWS);
                countRows++;
            }

            addNewTableRow();
            updateData("first", "true");

        }
        else if(str_first.equals("true"))
        {
            Log.d("DEFAULT","TRUE");
            String[] only_names = getArrayFromString(str_names);
            String[] only_times = getArrayFromString(str_times);
            String[] only_beeps = getArrayFromString(str_beeps);
            String[] only_reps  = getArrayFromString(str_reps);

            setTextOnViews(only_names, only_times, only_beeps, only_reps);
            addNewTableRow();


        }
    }

    private void setTextOnViews(String[] only_names, String[] only_times, String[] only_beeps, String[] only_reps) {
        for(int i=0;i<only_names.length;i++) {
            addNewTableRowWithText(only_names[i],only_times[i],only_beeps[i],only_reps[i]);
        }
    }

    private void addNewTableRowWithText(String only_name, String only_time, String only_beep, String only_rep) {
        ROWS = new TableRow(this);
        ROWS.setId(ID_ROW + countRows);
        int padding = (int) getResources().getDimension(R.dimen.padding);
        int paddingTop = (int) getResources().getDimension(R.dimen.paddingTop);
        ROWS.setPadding(padding, paddingTop, padding, 0);
        ROWS.setGravity(Gravity.CENTER_HORIZONTAL);

        CELLS = new EditText[4];
        for(int j=0;j<4;j++) {
            Log.d("ERROR",Integer.toString(j));
            String TEXT="";
            switch (j) {
                case 0:
                    TEXT = only_name;
                    break;
                case 1:
                    TEXT = only_time;
                    break;
                case 2:
                    TEXT = only_beep;
                    break;
                case 3:
                    TEXT = only_rep;
                    break;
            }

            CELLS[j] = new EditText(this);
            CELLS[j].setId(ID_CELL + countCells);
            CELLS[j].setText(TEXT);

            CELLS[j].setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT, 1f));
            CELLS[j].setOnTouchListener(new EditTextListener());
            CELLS[j].addTextChangedListener(new EditTextChangeListener());

            ROWS.addView(CELLS[j]);
            countCells++;
        }

        table.addView(ROWS);

        countRows++;
    }

    private void addNewTableRow() {

        ROWS = new TableRow(this);
        ROWS.setId(ID_ROW + countRows);
        int padding    = (int) getResources().getDimension(R.dimen.padding);
        int paddingTop = (int) getResources().getDimension(R.dimen.paddingTop);
        ROWS.setPadding(padding, paddingTop, padding, 0);
        ROWS.setGravity(Gravity.CENTER_HORIZONTAL);

        for(int j=0;j<4;j++) {
            String TEXT="";

            CELLS[j] = new EditText(this);
            CELLS[j].setId(ID_CELL + countCells);
            CELLS[j].setText(TEXT);

            CELLS[j].setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT, 1f));
            CELLS[j].setOnTouchListener(new EditTextListener());
            CELLS[j].addTextChangedListener(new EditTextChangeListener());

            ROWS.addView(CELLS[j]);
            countCells++;
        }

        table.addView(ROWS);

        countRows++;
    }

    private String[] getArrayFromString(String _s) {
        int count=0;
        char[] text = _s.toCharArray();

        Log.d("LOOL_TRUE", _s);
        for(int i=0;i<text.length;i++) {
            if( text[i] == (',') ) {
                count++;
            } else if((i+1) == text.length ){
                count++;
            }
        }
        String[] ARR = new String[count];

        count=0;
        String buf=""; String result="";
        for(int i=0;i<text.length;i++) {
            if( text[i] == (',') ) {
                result += buf;
                ARR[count] = result;

                count++;
                buf = result = "";
            } else if((i+1) == text.length ){
                buf += text[i];
                result += buf;

                ARR[count] = result;
                buf = result = "";
            } else {
                buf += text[i];
            }
        }

        return ARR;
    }

    public int[] getIntArrayFromString(String _s) {
        int count=0;
        char[] text = _s.toCharArray();

        Log.d("LOOL_TRUE", _s);
        for(int i=0;i<text.length;i++) {
            if( text[i] == (',') ) {
                count++;
            } else if((i+1) == text.length ){
                count++;
            }
        }
        int[] ARR = new int[count];

        count=0;
        String buf=""; String result="";
        for(int i=0;i<text.length;i++) {
            if( text[i] == (',') ) {
                int number=0;
                result += buf;
                try {
                    number = Integer.parseInt(result);
                } catch (Exception e) {

                }
                ARR[count] = number;

                count++;
                buf = result = "";
            } else if((i+1) == text.length ){
                int number=0;
                buf += text[i];
                result += buf;
                try {
                    number = Integer.parseInt(result);
                } catch (Exception e) {

                }
                ARR[count] = number;
                buf = result = "";
            } else {
                buf += text[i];
            }
        }

        return ARR;
    }


    private void setTextOnView(TextView _v, int[] _s, String _r) {
        String result = "";

        for (int i = 0; i < _s.length; i++) {
            if ((i + 1) == _s.length) {
                result += String.valueOf(_s[i]);
                break;
            }

            result += String.valueOf(_s[i]) + "\n";
        }
        _v.setText(result);

        result="";

        for (int i = 0; i < _s.length; i++) {
            if ((i + 1) == _s.length) {
                result += String.valueOf(_s[i]);
                break;
            }

            result += String.valueOf(_s[i]) + ",";
        }
        updateData(_r,result);
        Log.d("SHARED : "+_r, result);

    }

    private void setTextOnView(TextView _v, String[] _str, String _r) {
        String result = "";

        for (int i = 0; i < _str.length; i++) {
            if ((i + 1) == _str.length) {
                result += _str[i];
                break;
            }
            result += _str[i] + "\n";

        }
        _v.setText(result);

        result="";

        for (int i = 0; i < _str.length; i++) {
            if ((i + 1) == _str.length) {
                result += String.valueOf(_str[i]);
                break;
            }

            result += String.valueOf(_str[i]) + ",";
        }

        updateData(_r, result);
        Log.d("SHARED : " + _r, result);

    }

    private void setTextOnView(TextView _v, String _str) {
        String result = "";
        char[] text = _str.toCharArray();
        String buf = "";

        Log.d("LOOL_TRUE", _str);
        for(int i=0;i<text.length;i++) {
            if( text[i] == (',') ) {
                result += buf + "\n";
                buf = "";
            } else if((i+1) == text.length ){
                buf += text[i];
                result += buf;
                buf = "";
            } else {
                buf += text[i];
            }
        }

        Log.d("LOOL_TRUE_AFTER", result);
        _v.setText(result);
    }

    private void updateData(String key,String value) {
        SharedPreferences shared = getSharedPreferences("myData", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = shared.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public class EditTextListener implements View.OnTouchListener {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            currentEditor = (EditText) v;
            Log.d("ImD", Integer.toString(v.getId()));

            return false;
        }
    }

    public class EditTextChangeListener implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {}

        @Override
        public void afterTextChanged(Editable s) {
            Log.d("ID_CURRENT", Integer.toString(currentEditor.getId()-ID_CELL));
            int currentRow = (int) (currentEditor.getId()-ID_CELL) / 4;
            Log.d("EditText - ROW", Integer.toString(currentRow));

            if(checkCell(currentEditor.getId())) {
                Log.d("SAVE","CHECK SELL TRUE");
                saveAll();
            } else {
                Toast.makeText(getApplicationContext(), "Something wrong in last inputted field.",
                        Toast.LENGTH_LONG).show();
            }

            // ADD NEW LINE
            if(currentRow == countRows-1) {
                Log.d("EditText - ROW", "addNewTableRow();");
                addNewTableRow();
            }

            // REMOVE LINE
            if(currentRow == countRows-2) {
                if(checkRowOnEmpty(currentRow)) {
                    Log.d("REMOVE", "removeLastRow();");
                    removeLastRow();
                }
            }

        }
    }

    public void saveAll() {
        String N,T,B,R;
        N=T=B=R="";

        int rows = countRows-1;

        for(int i=0;i<4;i++) {
            int start_cell = ID_CELL+i;
            for(int j=0;j<rows;j++) {
                int ID = start_cell+j*4;
                String TEXT = ((EditText) findViewById(ID)).getText().toString();
                if( (j+1) == rows) {
                    switch (i) {
                        case 0:
                            N = N + TEXT;
                            break;
                        case 1:
                            T = T + TEXT;
                            break;
                        case 2:
                            B = B + TEXT;
                            break;
                        case 3:
                            R = R + TEXT;
                            break;
                    }
                    break;
                }
                switch (i) {
                    case 0:
                        N = N + TEXT + ",";
                        break;
                    case 1:
                        T = T + TEXT + ",";
                        break;
                    case 2:
                        B = B + TEXT + ",";
                        break;
                    case 3:
                        R = R + TEXT + ",";
                        break;
                }
            }
        }

        Log.d("STRINGS -N",N);
        Log.d("STRINGS -T",T);
        Log.d("STRINGS -B",B);
        Log.d("STRINGS -R",R);

        updateData("names", N);
        updateData("times", T);
        updateData("beeps", B);
        updateData("reps", R);
    }

    private class StartListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            SharedPreferences shared = getSharedPreferences("myData", Context.MODE_PRIVATE);
            String run = shared.getString("runningService", DEFAULT);


            // Check if service is running
            if (run.equals(DEFAULT) || run.equals("false")) {
                // check ALL statements
                if (checkAll()) {
                    // save ALL
                    saveAll();

                    // clear
                    updateData("i", DEFAULT);
                    updateData("j", DEFAULT);
                    updateData("id", DEFAULT);
                    updateData("stack_time", DEFAULT);

                    String ccc = shared.getString("stack_time", DEFAULT);
                    updateData("stack_time", DEFAULT);


                    // run service
                    startService(new Intent(MainActivity.this, MyToneService.class));
                    updateData("runningService","true");

                } else {
                    Toast.makeText(getApplicationContext(), "Something wrong in inputted fields.",
                            Toast.LENGTH_LONG).show();
                }

            } else {
                Toast.makeText(getApplicationContext(), "App is already running.",
                        Toast.LENGTH_LONG).show();
            }
        }
    }

    private class StopListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            // stop service
            stopService(new Intent(getBaseContext(), MyToneService.class));
            updateData("runningService","false");
        }
    }

    private class PrintListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            // move to another layout with listView

            Intent intent = new Intent(getBaseContext(),PrintActivity.class);
            startActivity(intent);
        }
    }

    private class AboutListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            // move to another about layout page

            Intent intent = new Intent(getBaseContext(),AboutActivity.class);
            startActivity(intent);
        }
    }

    public boolean checkCell(int _id) {

        int getColumn = (_id - ID_CELL) % 4;
        Log.d("COLUMN", Integer.toString(getColumn));

        if(getColumn != 0) {
            Log.d("COLUMN", "CHECK ON INT");
            boolean digitsOnly = TextUtils.isDigitsOnly(((EditText) findViewById(_id)).getText().toString());
            if(digitsOnly) {
                Log.d("COLUMN","CHECK ON INT - TRUE");
                return true;
            } else {
                Log.d("COLUMN","CHECK ON INT - FALSE");
                return false;
            }
        }

        if(((EditText)findViewById(_id)).getText().toString().equals("")) {
            Log.d("COLUMN","CHECK ON STRING - FALSE");
            return false;
        }
        Log.d("COLUMN","CHECK ON STRING - TRUE");
        return true;
    }

    public boolean checkColumn(int _column) {
        int startId = ID_CELL+_column;


        for(int i=0;i<countRows-1;i++) {
            boolean result = checkCell(startId+i*4);
            if(!result) {
                Log.d("COLUMN","CHECK ROW - FALSE");
                return false;
            }
        }

        Log.d("COLUMN","CHECK ROW - TRUE");
        return true;
    }

    public boolean checkRowOnEmpty(int _row) {
        int start_id = _row*4+ID_CELL;
        for(int i=0;i<4;i++) {
            if( !(((EditText)findViewById(start_id+i)).getText().toString().equals("")) ) {
                Log.d("ROW","CHECK ROW ON EMPTY - FALSE");
                return false;
            }
        }

        Log.d("ROW", "CHECK ROW ON EMPTY - TRUE");
        return true;
    }

    public void removeLastRow() {
        //boolean result = checkRowOnEmpty(countRows-1);
        //if(result) {
        Log.d("REMOVE","CHECK ROW ON EMPTY - TRUE");
        table.removeView(findViewById(ID_ROW + countRows-1));
        countRows--;
        countCells=countCells-4;
        //}
    }

    public boolean checkAll() {
        int end = countCells-4;
        for(int i=0;i<end;i++) {
            boolean result = checkCell(ID_CELL+i);
            if(!result) {
                Log.d("CHECK_ALL","FKING BAD");
                return false;
            }
        }

        Log.d("CHECK_ALL", "ALL FINE");
        return true;
    }

    @Override
    public void onBackPressed()
    {
        // code here to show dialog
        Intent a = new Intent(Intent.ACTION_MAIN);
        a.addCategory(Intent.CATEGORY_HOME);
        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
        //super.onBackPressed();  // optional depending on your needs
    }
}
