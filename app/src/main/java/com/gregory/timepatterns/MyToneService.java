package com.gregory.timepatterns;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.util.Log;

/**
 * Created by root on 1/21/16.
 */
public class MyToneService extends IntentService {
    public static final String DEFAULT = "NONE";
    // --------------------------------------------------------------------------------
    private final int duration = 1; // seconds
    private final double freqOfTone = 400; // hz
    private final int sampleRate = 8000;
    private final int numSamples = duration * sampleRate;
    private final double sample[] = new double[numSamples];
    private final byte generatedSnd[] = new byte[2 * numSamples];
    // --------------------------------------------------------------------------------
    boolean running;
    // --------------------------------------------------------------------------------
    public String str_first;
    public String str_names;
    public String str_times;
    public String str_beeps;
    public String str_reps;
    public String str_count;
    public String stack;
    public String d_day;

    public String[] names;
    public int[]    times;
    public int[]    beeps;
    public int[]    reps;
    // --------------------------------------------------------------------------------

    public MyToneService() {
        super("MyToneService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d("SERVICE", "go");


    }

    public void go() {

        running = true;
        Log.d("SERVICE", "go");
        new Thread(new Thread() {
            @Override
            public void run() {

                genTone();
                readData();
                for (int i = 0; i < names.length; i++) {  // for I = 1 to 99
                    if(running) {
                        try {
                            for (int j = 0; j < reps[i]; j++) { // for J = 1 to reps [I]
                                if(running) {
                                    notifyUser(names[i], i+1);
                                    Log.d("THREAD", Integer.toString(i));

                                    Thread.sleep(1000);
                                    for (int k = 0; k < beeps[i]; k++) { // for K = 1 to beeps [I]
                                        playTone(); // tone 400,150 % freq, dur (freq and dur on top)

                                        try {
                                            Thread.sleep(1000); // pause 100 (for java 1000 - 1 sec)
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }
                                    } // next K

                                    try {
                                        //Thread.sleep( (times[i]-beeps[i]) * 1000 ); // pause times[i] * 1000
                                        for(int z=0;z<times[i];z++) {
                                            Log.d("SLEEP",Integer.toString(z));
                                            Thread.sleep(1000);
                                        }
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                } // next J
                        }

                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                } // next I

                updateData("runningService", "false");
                stopSelf();
            }

        }).start();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //return super.onStartCommand(intent, flags, startId);
        Log.d("SERVICE", "onStartCommand");
        go();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.d("SERVICE", "onDestroy()");
        running = false;
        updateData("runningService", "false");
        super.onDestroy();
    }

    public void notifyUser(String _str, int _id) {

        int NOTIF_ID = _id;
        NotificationManager notifManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        Notification note = new Notification(R.drawable.ic_stat_ic_launcher, _str, System.currentTimeMillis());
        note.flags |= Notification.FLAG_ONGOING_EVENT;

        Intent notificationIntent = new Intent(this, PrintActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
        note.setLatestEventInfo(getBaseContext(), getResources().getString(R.string.app_name), _str, pendingIntent);
        //notifManager.notify(NOTIF_ID, note);

        //genTone();
        //playTone();
        SharedPreferences shared = getSharedPreferences("myData", Context.MODE_PRIVATE);
        String stack_time   = shared.getString("stack_time", DEFAULT);

        if(stack_time.equals(DEFAULT)) {
            updateData("stack_time", _str + ",");
        } else {
            String TEXT = stack_time + _str + ",";
            updateData("stack_time", TEXT);
        }
        startForeground(_id, note);

        Log.d("notifyUser", " notify ");
    }

    private void updateData(String key,String value) {
        SharedPreferences shared = getSharedPreferences("myData", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = shared.edit();
        editor.putString(key, value);
        editor.commit();
    }

    // generate tone
    void genTone() {
        // fill out the array
        for (int i = 0; i < numSamples; ++i) {
            sample[i] = Math.sin(2 * Math.PI * i / (sampleRate/freqOfTone));
        }

        // convert to 16 bit pcm sound array
        // assumes the sample buffer is normalised.
        int idx = 0;
        for (double dVal : sample) {
            short val = (short) (dVal * 32767);
            generatedSnd[idx++] = (byte) (val & 0x00ff);
            generatedSnd[idx++] = (byte) ((val & 0xff00) >>> 8);
        }
    }

    // play tone
    void playTone() {
        final AudioTrack audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC,
                8000, AudioFormat.CHANNEL_OUT_MONO,
                AudioFormat.ENCODING_PCM_16BIT, numSamples,
                AudioTrack.MODE_STATIC);
        audioTrack.write(generatedSnd, 0, numSamples);
        audioTrack.play();
    }

    public void readData() {
        SharedPreferences shared = getSharedPreferences("myData", Context.MODE_PRIVATE);
        str_first   = shared.getString("first", DEFAULT);
        str_names   = shared.getString("names", DEFAULT);
        str_times   = shared.getString("times", DEFAULT);
        str_beeps   = shared.getString("beeps", DEFAULT);
        str_reps    = shared.getString("reps", DEFAULT);
        str_count   = shared.getString("count", DEFAULT);

        names = getArrayFromString(str_names);
        times = getIntArrayFromString(str_times);
        beeps = getIntArrayFromString(str_beeps);
        reps  = getIntArrayFromString(str_reps);
    }

    private String[] getArrayFromString(String _s) {
        int count=0;
        char[] text = _s.toCharArray();

        Log.d("LOOL_TRUE", _s);
        for(int i=0;i<text.length;i++) {
            if( text[i] == (',') ) {
                count++;
            } else if((i+1) == text.length ){
                count++;
            }
        }
        String[] ARR = new String[count];

        count=0;
        String buf=""; String result="";
        for(int i=0;i<text.length;i++) {
            if( text[i] == (',') ) {
                result += buf;
                ARR[count] = result;

                count++;
                buf = result = "";
            } else if((i+1) == text.length ){
                buf += text[i];
                result += buf;

                ARR[count] = result;
                buf = result = "";
            } else {
                buf += text[i];
            }
        }

        return ARR;
    }

    public int[] getIntArrayFromString(String _s) {
        int count=0;
        char[] text = _s.toCharArray();

        Log.d("LOOL_TRUE", _s);
        for(int i=0;i<text.length;i++) {
            if( text[i] == (',') ) {
                count++;
            } else if((i+1) == text.length ){
                count++;
            }
        }
        int[] ARR = new int[count];

        count=0;
        String buf=""; String result="";
        for(int i=0;i<text.length;i++) {
            if( text[i] == (',') ) {
                int number=0;
                result += buf;
                try {
                    number = Integer.parseInt(result);
                } catch (Exception e) {

                }
                ARR[count] = number;

                count++;
                buf = result = "";
            } else if((i+1) == text.length ){
                int number=0;
                buf += text[i];
                result += buf;
                try {
                    number = Integer.parseInt(result);
                } catch (Exception e) {

                }
                ARR[count] = number;
                buf = result = "";
            } else {
                buf += text[i];
            }
        }

        return ARR;
    }

    private void getNoneAll() {
        updateData("i",  DEFAULT);
        updateData("j",  DEFAULT);
        updateData("id", DEFAULT);
    }

}
